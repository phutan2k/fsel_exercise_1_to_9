class HinhChuNhat {
  final double chieuDai;
  final double chieuRong;

  HinhChuNhat({required this.chieuDai,required this.chieuRong});

  void tinhChuVi() {
    print('Chu vi: ${(chieuDai + chieuRong) * 2}');
  }

  void tinhDienTich() {
    print('Dien tich: ${chieuRong * chieuDai}');
  }
}