import 'dart:math';

import 'package:fsel_exercise/rectangle/HinhChuNhat.dart';

class Exercise {
  void calcSum({required double a, required double b}) {
    print('Sum = ${a + b}');
  }

  void chuViVaDienTichHinhTron({required double banKinh}) {
    print('Chu vi hinh tron = ${2 * banKinh * pi}');
    print('Dien tich hinh tron = ${pi * pow(banKinh, 2)}');
  }

  void kiemTraChanLe({required int n}) {
    n % 2 == 0 ? print('$n la so chan') : print('$n la so le');
  }

  void daoNguocChuoi({required String text}) {
    String rs = '';

    for (int i = text.length - 1; i >= 0; i--) {
      var kyTu = text[i];
      rs += kyTu;
    }
    print('Chuoi sau khi dao nguoc: $rs');
  }

  void kiemTraSoNguyenTo({required int n}) {
    if (n < 2) {
      print('$n khong la so nguyen to');
    } else {
      if (n == 2) {
        print('$n la so nguyen to');
      } else {
        String rs = '';
        for (int i = 2; i <= n / 2; i++) {
          n % i == 0
              ? rs = '$n khong la so nguyen to'
              : rs = '$n la so nguyen to';
        }
        print(rs);
      }
    }
  }

  void timSoLonNhatTrongMang({required List<int> numberList}) {
    int max = numberList[0];

    for(int i = 1; i < numberList.length; i++) {
      if(numberList[i] > max) {
        max = numberList[i];
      }
    }
    print('So lon nhat trong mang la: $max');
  }

  void daoNguocMang({required List<dynamic> value}) {
    print('Mang bi dao nguoc: ${value.reversed}');
  }

  void tongCacSoChanTrongMang({required List<int> numberList}) {
    int sum = 0;

    for(int i =0; i<numberList.length; i++) {
      if(numberList[i] % 2 == 0) {
        sum += numberList[i];
      }
    }
    print('Tong cac so chan trong mang la: $sum');
  }
}

void main() {
  final exercise = Exercise();
  exercise.calcSum(a: 10, b: 5);
  exercise.chuViVaDienTichHinhTron(banKinh: 3.2);
  exercise.kiemTraChanLe(n: 3);
  exercise.daoNguocChuoi(text: 'Tan dep trai');
  exercise.kiemTraSoNguyenTo(n: 17);
  exercise.timSoLonNhatTrongMang(numberList: [8, 9, 4, 6, 17, 8, 34, 2, 1, 0]);
  exercise.daoNguocMang(value: ['Tan', 24, 'VietNam', true]);
  exercise.tongCacSoChanTrongMang(numberList: [2, 6, 8, 1, 3, 8, 9, 9, 0]);

  print('');

  final hcn = HinhChuNhat(chieuDai: 20, chieuRong: 12);
  hcn.tinhChuVi();
  hcn.tinhDienTich();
}